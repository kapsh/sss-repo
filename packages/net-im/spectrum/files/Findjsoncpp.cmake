find_package(PkgConfig)

pkg_search_module(JSONCPP REQUIRED jsoncpp)

#hack for non-default variable name
set(JSONCPP_LIBRARY ${JSONCPP_LIBRARIES})
