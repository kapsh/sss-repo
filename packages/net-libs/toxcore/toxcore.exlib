# Copyright 2018 Gluzskiy Alexandr <sss@sss.chaoslab.ru>
# Based in part upon 'tox-9999.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require github [ user=TokTok project=c-toxcore tag=v${PV} ]
#require scm-git
require systemd-service
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 1.11 ] need_libtool=false ]

export_exlib_phases src_configure src_install

SLOT="0"
SUMMARY="Encrypted P2P, messaging, and audio/video calling platform"
DESCRIPTION=""
HOMEPAGE="https://tox.chat"
LICENCES="GPL-3+"
MYOPTIONS="
        av [[ description = [ Enable audio/video support ] ]]
        daemon [[ description = [ Build and install tox network booststrap daemon ] ]]
        (
            log-debug [[ description = [ Turn on debug logging ] ]]
            log-error [[ description = [ Turn on error logging ] ]] 
            log-info [[ description = [ Turn on info logging ] ]] 
            log-trace [[ description = [ Turn on trace logging ] ]] 
            log-warn [[ description = [ Turn on warn logging ] ]] 
        )
        [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        av? ( media-libs/libvpx media-libs/opus )
        daemon? ( dev-libs/libconfig )
        dev-libs/libsodium
"

#require network
RESTRICT=("test")

toxcore_src_configure() {
    myconf=()
    if option log-debug; then
        myconf+=( --enable-logging )
        myconf+=( --with-log-level=DEBUG )
    elif option log-error; then
        myconf+=( --enable-logging )
        myconf+=( --with-log-level=ERROR )
    elif option log-info; then
        myconf+=( --enable-logging )
        myconf+=( --with-log-level=INFO )
    elif option log-trace; then
        myconf+=( --enable-logging )
        myconf+=( --with-log-level=TRACE )
    elif option log-warn; then
        myconf+=( --enable-logging )
        myconf+=( --with-log-level=WARNING )
    fi
     econf \
        $(option_enable av av ) \
        $(option_enable daemon daemon ) \
        $(option_enable daemon dht-bootstrap ) \
        "${myconf[@]}"
        
}

toxcore_src_install() {
    default_src_install
    if option daemon; then
        install_systemd_files
    else
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin
    fi
}

